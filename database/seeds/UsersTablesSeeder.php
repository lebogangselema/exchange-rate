<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\User;

class UsersTablesSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        User::create([
            'name'    => 'Pam',
            'email'    => 'pam_Laravel@gmail.com',
            'password'   =>  Hash::make('password'),
            'currency'   =>  'USD',
            'exchange_rate'   =>  1
        ]);
    }
}
