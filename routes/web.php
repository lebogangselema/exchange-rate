<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('login', function () {
    return view('login');
});

Route::get('currency', array (
    'uses' => 'MainController@exchange_rates'
//))->middleware('auth');
));

Route::post('login', array(
    'uses' => 'MainController@doLogin'
));

Route::post('/update/exchangerate', array(
    'uses'=>'MainController@update_exchange_rates'
));