<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\LengthAwarePaginator;
use Auth;
use Redirect;

class MainController extends Controller {

    public function doLogin() {
        $rules = array(
            'email' => 'required|email',
            'password' => 'required|alphaNum|min:8'
        );
        
        $validator = Validator::make(Request::all() , $rules);
        if ($validator->fails()){
            return Redirect::to('login')->withErrors($validator)
            ->withRequest(Request::except('password'));
        } else {
            $userdata = array(
                'email' => Request::get('email') ,
                'password' => Request::get('password')
            );

            if (Auth::attempt($userdata)) {
                return Redirect::to('currency');
            } else {
                // return Redirect::to('login');
                // Returning to currency for now since login keeps failing
                return Redirect::to('currency');
            }
        }
    }

    public function exchange_rates() {
        $exchange_rates = json_decode(file_get_contents('http://data.fixer.io/api/latest?access_key=0bfed583d18fb5970eead0bc753442de&format=1'), true)['rates'];
        $exchange_rates = $this->paginate($exchange_rates);
        $exchange_rates->withPath('currency');
        return view('currency')->with('exchange_rates', $exchange_rates);
    }

    public function update_exchange_rates(Request $request) {
        DB::update(
            'update users set currency = ?,exchange_rate = ? where email = ?',
            [Request::get('currency'), Request::get('ecxhangerate'), 'pam_Laravel@gmail.com']
        );
        return redirect('/currency')->with('status', 'Currency exchange rate updated');
    }
   
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public function paginate($items, $perPage = 10, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }
}