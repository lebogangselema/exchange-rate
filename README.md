## Cloning the environemnt

- Clone the code from repo using : git clone https://lebogangselema@bitbucket.org/lebogangselema/exchange-rate.git
- Create a database on your local
- Make changes to the .env to connect to your database
- Run php artisan migrate
- Run php artisan db:seed
- Run php artisan serve
