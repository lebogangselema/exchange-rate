<!doctype html>
<html>
<head>
<title>Exhange Rate | Currency</title>
</head>
<body style='text-align: center'>
    <!-- 0bfed583d18fb5970eead0bc753442de -->
    <h1>Currencies and Exchange rates</h1>

    <div class="current-currency-rate">
        <form action="/update/exchangerate" method="POST">
            @csrf
            <div class="form-group">
                <label for="currency">Currency</label>
                <input type="text" name="currency" class="form-control" placeholder="Currency">
            </div>
            <br>
            <div class="form-group">
                <label for="ecxhangerate">Exchange Rate</label>
                <input type="text" name="ecxhangerate" placeholder="Exchange Rate" class="form-control"></text>
            </div>
            <br>
            <button type="submit" value="Submit" class="btn btn-primary">Submit </button>
        </form>
    </div>

    <table style="width:100%">
        <tr>
            <th><h2>Currency</h2></th>
            <th><h2>Exchange Rate</h2></th>
        </tr>
        @foreach ($exchange_rates as $key => $exchange_rate)
        <tr>
            <td>{{ $key }} </td>
            <td>{{ $exchange_rate }}</td>
        </tr>
        @endforeach
    </table>
    {{ $exchange_rates->links() }}
</body>
<style>
table {
  border-collapse: collapse;
}
td {
    background: #e8edff;
    border-right: 1px solid #aabcfe;
    border-left: 1px solid #aabcfe;
    color: #669;
    padding: 8px;
}
td {
    border-bottom: 1px solid #ccc;
    color: #669;
    padding: 6px 8px;
}
tbody tr:nth-child(2n) td {
    background-color: #f7f7f7;
}
th {
    font-size: 14px;
    font-weight: 400;
    color: #039;
    border-bottom: 2px solid #6678b1;
    padding: 10px 8px;
}

input {
    height: 30px;
}

button.btn.btn-primary {
    height: 35px;
    width: 100px;
}
</style>