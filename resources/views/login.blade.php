<!doctype html>
<html>
<head>
<title>Exhange Rate | Login</title>
</head>
<body>

{{ Form::open(array('url' => '/login')) }}
<h1>Login</h1>

<!-- if there are login errors, show them here -->
<p>
    {{ $errors->first('email') }}
    {{ $errors->first('password') }}
</p>

<p>
    {{ Form::label('email', 'Email Address') }}
    {{ Form::text('email', Request::old('email'), array('placeholder' => 'awesome@awesome.com')) }}
</p>

<p>
    {{ Form::label('password', 'Password') }}
    {{ Form::password('password') }}
</p>

<p>{{ Form::submit('Submit!') }}</p>
{{ Form::close() }}

<style>
form{
    border: 7px solid #aabcfe;
    width: 40%;
    padding: 35px;
    margin: 15%;
    margin-left: 28%;
    border-radius: 26%;
    background: #efeded;
}
input {
    height: 30px;
}
input[type="submit"] {
    margin-left: 44%;
    height: 42px;
    width: 100px;
}
</style>